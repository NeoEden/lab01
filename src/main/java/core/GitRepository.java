package core;
import java.io.*;
import java.nio.file.*;
public class GitRepository {
	private String valore;

	public GitRepository (String s){
		valore=s;
	} 
	
	public String getHeadRef () throws Exception {
		Path p = Paths.get(valore);
		BufferedReader bf=new BufferedReader(new FileReader(valore+"/HEAD"));
		return bf.readLine().substring(5);
	}
}
